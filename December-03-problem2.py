# the spiral
# 37  36  35  34  33  32  31
# 38  17  16  15  14  13  30
# 39  18   5   4   3  12  29
# 40  19   6   1   2  11  28
# 41  20   7   8   9  10  27
# 42  21  22  23  24  25  26
# 43  44  45  46  47  48  49

# lower right number per square
# 1 -> 9 -> 25 -> 49
# 1^2  3^2  5^2   7^2

# coordinates
# 1 = 0,0
# 2 = 0,1
# 11 = 0,2
# 12 = 1,2
# 10 = -1,2

# first number in a square at size n (with n being 1, 3, 5, 7, etc): (n-2)^2 + 1
# last number in square at size n: n^2
# (n+2)^2 - n^2 always divisble by 4
# (n+2)*(n+2) - n^2 = n^2+4n+4 - n^2 = 4n+4 = 4(n+1) hence always divisible by 4
# if x is in the n-th square, x value is (n-2)^2 + 1 <= x <= n^2
# relValueInSquare = x - ((n-2)^2 + 1)
# squareQuadrant = relValueInSquare / n
# 

import math
firstNumberLargerThan = 277678

summedBoard = {(0,0): 1}

adjacents = [(1,0), (1,1), (0,1), (-1,1), (-1, 0), (-1, -1), (0, -1), (1,-1)]

def sumSurroundingValues(coord):
    sum = 0
    for adjacent in adjacents:
        evaluateCoord = (coord[0] + adjacent[0], coord[1] + adjacent[1])
        evaluate = summedBoard.get(evaluateCoord)
        if (evaluate != None):
            sum += evaluate
    return sum

currentCoordinate = (0, 0)
currentNumber = 1
currentSquare = 1

amountUp = 0
amountLeft = 0
amountDown = 0
amountRight = 0

while True:
    currentNumber += 1
    
    # update coordinate and square
    if amountRight == 0:
        currentCoordinate = (currentCoordinate[0] + 1, currentCoordinate[1])
        currentSquare += 2
        
        amountUp = currentSquare - 2
        amountLeft = currentSquare - 1
        amountDown = currentSquare - 1
        amountRight = currentSquare - 1
    else:
        if amountUp > 0:
            amountUp -= 1 
            currentCoordinate = (currentCoordinate[0], currentCoordinate[1] + 1)
        else:
            if amountLeft > 0:
                amountLeft -= 1 
                currentCoordinate = (currentCoordinate[0] - 1, currentCoordinate[1])
            else:
                if amountDown > 0:
                    amountDown -= 1 
                    currentCoordinate = (currentCoordinate[0], currentCoordinate[1] - 1)
                else:
                    #amountRight > 0
                    amountRight -= 1
                    currentCoordinate = (currentCoordinate[0] + 1, currentCoordinate[1])
                    
    summedBoard[currentCoordinate] = sumSurroundingValues(currentCoordinate)
    if summedBoard[currentCoordinate] > firstNumberLargerThan:
        break;

print(currentCoordinate, ": ", summedBoard[currentCoordinate])

#print(abs(currentCoordinate[0]) + abs(currentCoordinate[1]))
