# Memory Reallocation

memoryBank =  [14, 0, 15, 12, 11, 11, 3, 5, 1, 6, 8, 4, 9, 1, 8, 4]
#memoryBank = [0, 2, 7, 0]  #test input

def findMemoryBankIndexWithMostBlocks():
    maxBlocks = -1
    maxBlocksMemoryBankIndex = -1
    for i in range(0, len(memoryBank)):
        if memoryBank[i] > maxBlocks:
            maxBlocks = memoryBank[i]
            maxBlocksMemoryBankIndex = i
    return maxBlocksMemoryBankIndex

def spreadBlocksToBanks(index, amountOfBlocks):
    index = index % len(memoryBank)
    while amountOfBlocks > 0:
        amountOfBlocks -= 1
        memoryBank[index] += 1
        index = (index + 1) % len(memoryBank)

seenMemoryBanks = set()
seenMemoryBanks.add(str(memoryBank))
previousSeenMemoryBanksCount = 0
steps = 0
while previousSeenMemoryBanksCount < len(seenMemoryBanks):
    previousSeenMemoryBanksCount = len(seenMemoryBanks)
    distributeMemoryBankIndex = findMemoryBankIndexWithMostBlocks()
    toDistributeBlocks = memoryBank[distributeMemoryBankIndex]
    memoryBank[distributeMemoryBankIndex] = 0
    spreadBlocksToBanks(distributeMemoryBankIndex + 1, toDistributeBlocks)
    seenMemoryBanks.add(str(memoryBank))
    steps += 1
print(steps)
