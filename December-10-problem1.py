# Knot Hash

circularList = list(range(0, 256))
input = 227,169,3,166,246,201,0,47,1,255,2,254,96,3,97,144
#input = [3, 4, 1, 5]  # test input
positionIndex = 0
skipSize = 0

def getSubList(index, length):
    global circularList
    if index >= len(circularList):
        print("input index in getSubList should be in range of circularList")
    subList = circularList[index:index+length]
    if len(subList) < length:
        amountFromFront = length - len(subList)
        subList.extend(circularList[:amountFromFront])
    return subList

def replaceSubList(index, subList):
    global circularList
    index = (index) % len(circularList)
    for entry in subList:
        circularList[index] = entry
        index = (index + 1) % len(circularList)

print(circularList)
for entry in input:
    subList = getSubList(positionIndex, entry)
    subList.reverse()
    replaceSubList(positionIndex, subList)
    positionIndex = (positionIndex + entry + skipSize) % len(circularList)
    skipSize += 1
    print(circularList)
    
print("Answer:", circularList[0] * circularList[1])


