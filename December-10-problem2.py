# Knot Hash

circularList = list(range(0, 256))
input = "227,169,3,166,246,201,0,47,1,255,2,254,96,3,97,144"
inputAppendLengths = [17, 31, 73, 47, 23]
#input = [3, 4, 1, 5]  # test input
positionIndex = 0
skipSize = 0

def getSubList(index, length):
    global circularList
    if index >= len(circularList):
        print("input index in getSubList should be in range of circularList")
    subList = circularList[index:index+length]
    if len(subList) < length:
        amountFromFront = length - len(subList)
        subList.extend(circularList[:amountFromFront])
    return subList

def replaceSubList(index, subList):
    global circularList
    index = (index) % len(circularList)
    for entry in subList:
        circularList[index] = entry
        index = (index + 1) % len(circularList)

def hashBlockAt(index):
    global circularList
    hash = 0
    for i in range(index, index+16):
        hash = hash ^ circularList[i]
    return hash

#print(circularList)
inputLengths = []
for char in input:
    inputLengths.append(ord(char))
inputLengths.extend(inputAppendLengths)

for i in range(0,64):
    for length in inputLengths:
        subList = getSubList(positionIndex, length)
        subList.reverse()
        replaceSubList(positionIndex, subList)
        positionIndex = (positionIndex + length + skipSize) % len(circularList)
        skipSize += 1

print(circularList)
hash = ""
for i in range(0,16):
    blockHash = hashBlockAt(i*16)
    hash += hex(blockHash)[2:].zfill(2)

print("Answer:", hash)


