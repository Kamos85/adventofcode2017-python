# Packet Scanners

input = [(0, 3),
(1, 2),
(2, 4),
(4, 4),
(6, 5),
(8, 6),
(10, 6),
(12, 6),
(14, 6),
(16, 8),
(18, 8),
(20, 8),
(22, 8),
(24, 10),
(26, 8),
(28, 8),
(30, 12),
(32, 14),
(34, 12),
(36, 10),
(38, 12),
(40, 12),
(42, 9),
(44, 12),
(46, 12),
(48, 12),
(50, 12),
(52, 14),
(54, 14),
(56, 14),
(58, 12),
(60, 14),
(62, 14),
(64, 12),
(66, 14),
(70, 14),
(72, 14),
(74, 14),
(76, 14),
(80, 18),
(88, 20),
(90, 14),
(98, 17)]

#test input
#input = [(0, 3), (1, 2), (4, 4), (6, 4)]

simulationSteps = input[len(input)-1][0] + 1 # the last registered layer
securityState = dict()
currentPacketLayer = -1
delay = -1

# register at which delays we were caught with the
# depth of the layer so we know not to run the simulation at that point
caughtOptimization = []

def buildInitialSecurityState():
    global input
    global securityState
    
    securityState.clear()
    for entry in input:
        secuEntry = {"Pos": 0, "Dir": 1, "Depth": entry[1]}
        securityState[entry[0]] = secuEntry

def updateSecurityState():
    global securityState
    for key,value in securityState.items():
        if (value["Pos"] + value["Dir"]) < 0:
            value["Dir"] = 1
        if (value["Pos"] + value["Dir"]) >= value["Depth"]:
            value["Dir"] = -1
        value["Pos"] = value["Pos"] + value["Dir"]

caught = True
prematureCatch = False

while caught:
    # start from scratch, reset values
    currentPacketLayer = -1
    if not prematureCatch:  # when prematureCatch is true then we dont have to rebuild the initial security state
        buildInitialSecurityState()
    caught = False
    #delay
    delay += 1
    
    #before running the simulation, check the caughtOptimization
    prematureCatch = False
    for caughtEntry in caughtOptimization:
        if ((delay - caughtEntry[0]) % caughtEntry[1]) == 0:
            #print("premature catch!", delay, "by", caughtEntry)
            prematureCatch = True
            break
    
    if prematureCatch:
        caught = True
        continue
    
    for i in range(0, delay):
        updateSecurityState()
        
    # update security state
    for i in range(0, simulationSteps):
        currentPacketLayer += 1
        # check if caught
        packetLayerSecurityState = securityState.get(currentPacketLayer)
        if packetLayerSecurityState != None:
            if packetLayerSecurityState["Pos"] == 0: #caught
                caught = True
                caughtOptimization.append((delay, (packetLayerSecurityState["Depth"]-1) * 2)) # the period of a scanner (d-1)*2
                print("caught in layer:", currentPacketLayer, (delay, (packetLayerSecurityState["Depth"]-1) * 2))
                break
        updateSecurityState()

print("Answer:", delay)
