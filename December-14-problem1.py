# Disk Defragmentation

class KnotHash:
    def getSubList(self, index, length):
        if index >= len(self.circularList):
            print("input index in getSubList should be in range of circularList")
        subList = self.circularList[index:index+length]
        if len(subList) < length:
            amountFromFront = length - len(subList)
            subList.extend(self.circularList[:amountFromFront])
        return subList

    def replaceSubList(self, index, subList):
        index = (index) % len(self.circularList)
        for entry in subList:
            self.circularList[index] = entry
            index = (index + 1) % len(self.circularList)

    def hashBlockAt(self, index):
        hash = 0
        for i in range(index, index+16):
            hash = hash ^ self.circularList[i]
        return hash

    def hash(self, key):
        self.circularList = list(range(0, 256))
        self.inputAppendLengths = [17, 31, 73, 47, 23]
        self.positionIndex = 0
        self.skipSize = 0
        
        inputLengths = []
        for char in key:
            inputLengths.append(ord(char))
        inputLengths.extend(self.inputAppendLengths)

        for i in range(0,64):
            for length in inputLengths:
                subList = self.getSubList(self.positionIndex, length)
                subList.reverse()
                self.replaceSubList(self.positionIndex, subList)
                self.positionIndex = (self.positionIndex + length + self.skipSize) % len(self.circularList)
                self.skipSize += 1

        hash = ""
        for i in range(0,16):
            blockHash = self.hashBlockAt(i*16)
            hash += hex(blockHash)[2:].zfill(2)

        return hash

knotHasher = KnotHash()
amountOf1s = 0;
for i in range(0,128):
    rowHash = knotHasher.hash("hfdlxzhv-" + str(i));
    binaryForm = bin(int(rowHash, 16))[2:].zfill(128)
    amountOf1s += str(binaryForm).count("1")

print("Answer", amountOf1s)

