# Disk Defragmentation

class KnotHash:
    def getSubList(self, index, length):
        if index >= len(self.circularList):
            print("input index in getSubList should be in range of circularList")
        subList = self.circularList[index:index+length]
        if len(subList) < length:
            amountFromFront = length - len(subList)
            subList.extend(self.circularList[:amountFromFront])
        return subList

    def replaceSubList(self, index, subList):
        index = (index) % len(self.circularList)
        for entry in subList:
            self.circularList[index] = entry
            index = (index + 1) % len(self.circularList)

    def hashBlockAt(self, index):
        hash = 0
        for i in range(index, index+16):
            hash = hash ^ self.circularList[i]
        return hash

    def hash(self, key):
        self.circularList = list(range(0, 256))
        self.inputAppendLengths = [17, 31, 73, 47, 23]
        self.positionIndex = 0
        self.skipSize = 0
        
        inputLengths = []
        for char in key:
            inputLengths.append(ord(char))
        inputLengths.extend(self.inputAppendLengths)

        for i in range(0,64):
            for length in inputLengths:
                subList = self.getSubList(self.positionIndex, length)
                subList.reverse()
                self.replaceSubList(self.positionIndex, subList)
                self.positionIndex = (self.positionIndex + length + self.skipSize) % len(self.circularList)
                self.skipSize += 1

        hash = ""
        for i in range(0,16):
            blockHash = self.hashBlockAt(i*16)
            hash += hex(blockHash)[2:].zfill(2)

        return hash

grid = [[0 for i in range(128)] for i in range(128)]

def createRegionAt(y, x, groupNumber):
    global grid
    if x < 0 or x >= 128 or y < 0 or y >= 128:
        return  #out of grid
    
    if grid[y][x] != -1:
        return  #region already assigned or empty spot
    
    grid[y][x] = groupNumber
    createRegionAt(y-1, x, groupNumber)
    createRegionAt(y+1, x, groupNumber)
    createRegionAt(y, x-1, groupNumber)
    createRegionAt(y, x+1, groupNumber)
    
knotHasher = KnotHash()

for i in range(128):
    rowHash = knotHasher.hash("hfdlxzhv-" + str(i));
    binaryForm = bin(int(rowHash, 16))[2:].zfill(128)
    for j in range(128):
        if binaryForm[j] == '1':
            grid[i][j] = -1  # -1 signals no group
        else:
            grid[i][j] = 0

groupNumber = 0;
for y in range(128):
    for x in range(128):
        if grid[y][x] == -1:
            groupNumber += 1
            createRegionAt(y, x, groupNumber)
            
print("Answer:", groupNumber);
