# Dueling Generators

GeneratorAPrevValue = 679
GeneratorAFactor = 16807

GeneratorBPrevValue = 771
GeneratorBFactor = 48271

matches = 0
for i in range(5*1000000):
    GeneratorAPrevValue = (GeneratorAPrevValue * GeneratorAFactor) % 2147483647
    while (GeneratorAPrevValue & 3) != 0:
        GeneratorAPrevValue = (GeneratorAPrevValue * GeneratorAFactor) % 2147483647
    GeneratorBPrevValue = (GeneratorBPrevValue * GeneratorBFactor) % 2147483647
    while (GeneratorBPrevValue & 7) != 0:
        GeneratorBPrevValue = (GeneratorBPrevValue * GeneratorBFactor) % 2147483647
    
    if (GeneratorAPrevValue & 65535) == (GeneratorBPrevValue & 65535):
        matches += 1

print("Answer:", matches)
