# Permutation Promenade
import re

filename = "December-16-input.txt"
file = open(filename, "r")
input = file.read()
file.close()

instructions = input.split(",")
lineup = list("abcdefghijklmnop")

# test
#instructions = "s1,x3/4,pe/b".split(",")
#lineup = list("abcde")

def spin(count):
    global lineup
    end = lineup[:-count]
    lineup = lineup[-count:]
    lineup.extend(end)

def exchange(pos1, pos2):
    global lineup
    temp = lineup[pos1]
    lineup[pos1] = lineup[pos2]
    lineup[pos2] = temp

def partner(letter1, letter2):
    global lineup
    pos1 = lineup.index(letter1)
    pos2 = lineup.index(letter2)
    exchange(pos1, pos2)

for instruction in instructions:
    matchObj = re.match( r'([sxp])([0-9a-p]+)/?([0-9a-p]+)?', instruction)
    if matchObj == None:
        print("Could not parse instruction:", instruction)
        continue
    operation = matchObj.group(1)
    if (operation == "s"):
        spin(int(matchObj.group(2)))
    else:
        if (operation == "x"):
            exchange(int(matchObj.group(2)), int(matchObj.group(3)))
        else:
            partner(matchObj.group(2), matchObj.group(3))

print("".join(lineup))
