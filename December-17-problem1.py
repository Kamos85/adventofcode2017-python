# Spinlock

input = 371
# test: input = 3

spinList = [0]

currentValue = 1
currentPosition = 0

while currentValue <= 2017:
    currentPosition = (currentPosition + input) % len(spinList)
    currentPosition += 1
    spinList.insert(currentPosition, currentValue)
    currentValue += 1

print(spinList[currentPosition - 2])
print(spinList[currentPosition - 1])
print(spinList[currentPosition])
print("Answer", spinList[currentPosition + 1])
print(spinList[currentPosition + 2])