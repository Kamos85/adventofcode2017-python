# Spinlock

input = 371
#input = 3

currentValue = 1
currentPosition = 0
valueAfter0 = 0;
while currentValue <= 50000000:
    currentPosition = (currentPosition + input) % currentValue
    currentPosition += 1
    if currentPosition == 1:
        valueAfter0 = currentValue
    currentValue += 1

print("Answer", valueAfter0)