# Duet
import re

input = ["set i 31",
"set a 1",
"mul p 17",
"jgz p p",
"mul a 2",
"add i -1",
"jgz i -2",
"add a -1",
"set i 127",
"set p 826",
"mul p 8505",
"mod p a",
"mul p 129749",
"add p 12345",
"mod p a",
"set b p",
"mod b 10000",
"snd b",
"add i -1",
"jgz i -9",
"jgz a 3",
"rcv b",
"jgz b -1",
"set f 0",
"set i 126",
"rcv a",
"rcv b",
"set p a",
"mul p -1",
"add p b",
"jgz p 4",
"snd a",
"set a b",
"jgz 1 3",
"snd b",
"set f 1",
"add i -1",
"jgz i -11",
"snd a",
"jgz f -16",
"jgz a -19"]

#test input
##input = ["set a 1",
##"add a 2",
##"mul a a",
##"mod a 5",
##"snd a",
##"set a 0",
##"rcv a",
##"jgz a -1",
##"set a 1",
##"jgz a -2"]

registers = {}

instructionCounter = 0
playedFrequency = 0

# function to convert a string like "123" to an int or if the
# operant is a register name returns the value of the register
def getOperantAsValue(stringOperant):
    global registers
    try:
        value = int(stringOperant)
    except:
        value = registers.get(stringOperant, 0)
    return value
    

while instructionCounter >= 0 and instructionCounter < len(input):
    matchObj = re.match( r'(snd|set|add|mul|mod|rcv|jgz) (\-?[0-9a-z]+).?(\-?[0-9a-z]+)?', input[instructionCounter])
    
    if matchObj == None:
        print("Could not parse instruction:", instruction)
        continue

    opcode = matchObj.group(1)
    operant1 = matchObj.group(2)
    operant2 = matchObj.group(3)
    print("executing:", opcode, operant1, operant2)

    if opcode == "snd":
        playedFrequency = getOperantAsValue(operant1)
        instructionCounter += 1
        continue
    
    if opcode == "set":
        registers[operant1] = getOperantAsValue(operant2)
        instructionCounter += 1
        continue

    if opcode == "add":
        registerValue = registers.get(operant1, 0)
        registerValue += getOperantAsValue(operant2)
        registers[operant1] = registerValue
        instructionCounter += 1
        continue

    if opcode == "mul":
        registerValue = registers.get(operant1, 0)
        registerValue *= getOperantAsValue(operant2)
        registers[operant1] = registerValue
        instructionCounter += 1
        continue

    if opcode == "mod":
        registerValue = registers.get(operant1, 0)
        registerValue %= getOperantAsValue(operant2)
        registers[operant1] = registerValue
        instructionCounter += 1
        continue
    
    if opcode == "rcv":
        value = getOperantAsValue(operant1)
        if value != 0:
            break
        instructionCounter += 1
        continue
    
    if opcode == "jgz":
        conditionValue = getOperantAsValue(operant1)
        if conditionValue > 0:
            instructionCounter += getOperantAsValue(operant2)
        else:
            instructionCounter += 1
        continue;
    
print("Answer:", playedFrequency)
