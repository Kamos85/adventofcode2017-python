# Duet
import re

input = ["set i 31",
"set a 1",
"mul p 17",
"jgz p p",
"mul a 2",
"add i -1",
"jgz i -2",
"add a -1",
"set i 127",
"set p 826",
"mul p 8505",
"mod p a",
"mul p 129749",
"add p 12345",
"mod p a",
"set b p",
"mod b 10000",
"snd b",
"add i -1",
"jgz i -9",
"jgz a 3",
"rcv b",
"jgz b -1",
"set f 0",
"set i 126",
"rcv a",
"rcv b",
"set p a",
"mul p -1",
"add p b",
"jgz p 4",
"snd a",
"set a b",
"jgz 1 3",
"snd b",
"set f 1",
"add i -1",
"jgz i -11",
"snd a",
"jgz f -16",
"jgz a -19"]

#test input
##input = ["snd 1",
##"snd 2",
##"snd p",
##"rcv a",
##"rcv b",
##"rcv c",
##"rcv d"]


class Program:    
    def __init__(self, programNumber, receiveQueue, sendQueue):
        self.programNumber = programNumber
        self.receiveQueue = receiveQueue
        self.sendQueue = sendQueue
        self.registers = {"p": programNumber}
        self.instructionCounter = 0
        self.valuesSent = 0

    # function to convert a string like "123" to an int or if the
    # operant is a register name returns the value of the register
    def getOperantAsValue(self, stringOperant):
        try:
            value = int(stringOperant)
        except:
            value = self.registers.get(stringOperant, 0)
        return value

    def step(self):
        global input
        
        if self.instructionCounter < 0 or self.instructionCounter >= len(input):
            return False

        matchObj = re.match( r'(snd|set|add|mul|mod|rcv|jgz) (\-?[0-9a-z]+).?(\-?[0-9a-z]+)?', input[self.instructionCounter])
    
        if matchObj == None:
            print("Could not parse instruction:", instruction)
            return False

        opcode = matchObj.group(1)
        operant1 = matchObj.group(2)
        operant2 = matchObj.group(3)
        #print("executing (" + str(self.programNumber) + "):", opcode, operant1, operant2)

        if opcode == "snd":
            self.sendQueue.append(self.getOperantAsValue(operant1))
            self.valuesSent += 1
            self.instructionCounter += 1
            return True
        
        if opcode == "set":
            self.registers[operant1] = self.getOperantAsValue(operant2)
            self.instructionCounter += 1
            return True

        if opcode == "add":
            registerValue = self.registers.get(operant1, 0)
            registerValue += self.getOperantAsValue(operant2)
            self.registers[operant1] = registerValue
            self.instructionCounter += 1
            return True

        if opcode == "mul":
            registerValue = self.registers.get(operant1, 0)
            registerValue *= self.getOperantAsValue(operant2)
            self.registers[operant1] = registerValue
            self.instructionCounter += 1
            return True

        if opcode == "mod":
            registerValue = self.registers.get(operant1, 0)
            registerValue %= self.getOperantAsValue(operant2)
            self.registers[operant1] = registerValue
            self.instructionCounter += 1
            return True
        
        if opcode == "rcv":
            if len(self.receiveQueue) == 0:
                return False
            self.registers[operant1] = self.receiveQueue[0]
            self.receiveQueue.remove(self.receiveQueue[0])
            self.instructionCounter += 1
            return True
        
        if opcode == "jgz":
            conditionValue = self.getOperantAsValue(operant1)
            if conditionValue > 0:
                self.instructionCounter += self.getOperantAsValue(operant2)
            else:
                self.instructionCounter += 1
            return True

sendQueueProgram0 = []
sendQueueProgram1 = []

program0 = Program(0, sendQueueProgram1, sendQueueProgram0)
program1 = Program(1, sendQueueProgram0, sendQueueProgram1)

program0Stepped = True
program1Stepped = True

while program0Stepped or program1Stepped:
    program0Stepped = program0.step()
    program1Stepped = program1.step()
    #print("Stepped: ", program0Stepped, program1Stepped)

print("DEADLOCKED, Answer:", program1.valuesSent)
