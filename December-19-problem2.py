# A Series of Tubes

class Direction:
    UP = 1
    RIGHT = 2
    DOWN = 3
    LEFT = 4

filename = "December-19-input.txt"
file = open(filename, "r")
input = file.read()
file.close()

lines = input.split("\n")

y = 0
x = lines[y].index("|")
direction = Direction.DOWN
steps = 1

while True:
    if direction == Direction.UP:
        y -= 1
    if direction == Direction.RIGHT:
        x += 1
    if direction == Direction.DOWN:
        y += 1
    if direction == Direction.LEFT:
        x -= 1
        
    steps += 1
    
    currentChar = lines[y][x]
    if currentChar == " ":
        steps -= 1 # went one step too far
        break

    if currentChar == "|" or currentChar == "-":
        continue
    
    if currentChar == "+":
        if direction == Direction.UP or direction == Direction.DOWN:
            if lines[y][x+1] != " ":
                direction = Direction.RIGHT
            else:
                direction = Direction.LEFT
        else: # direction == Direction.LEFT or direction == Direction.RIGHT
            if lines[y+1][x] != " ":
                direction = Direction.DOWN
            else:
                direction = Direction.UP
        continue

print("Answer:", steps)
