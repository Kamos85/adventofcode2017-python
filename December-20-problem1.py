# Particle Swarm
import re
import math

filename = "December-20-input.txt"
file = open(filename, "r")
input = file.read()
file.close()

lines = input.split("\n")

class Vector3:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    # Used for debugging. This method is called when you print an instance  
    def __str__(self):
        return "(" + str(self.x) + ", " + str(self.y) + ", " + str(self.z) + ")"

    def get_length(self):
        return math.sqrt(self.x ** 2 + self.y ** 2 + self.z ** 2)

    def __add__(self, v):
        return Vector3(self.x + v.x, self.y + v.y, self.z + v.z)

    def __sub__(self, v):
        return Vector3(self.x - v.x, self.y - v.y, self.z - v.z)

    def __mul__(self, n):
        return Vector3(self.x * n, self.y * n, self.z * n)
    
    def get_manhattan_length(self):
        return abs(self.x) + abs(self.y) + abs(self.z)
    
##class particle:
##    def __init__(self, position, velocity, acceleration):
##        self.position = position
##        self.velocity = velocity
##        self.acceleration = acceleration
##    
##    def step(self):
# commented above code because of new realization: particle that will stay closest to
# 0 is the particle that has the least acceleration

smallestAcceleratedParticle = -1
smallestAccelerationML = 99999
smallestAccelerationVelocityML = 99999

currentParticle = 0
for particleConfig in lines:
    matchObj = re.match( r'p=<(\-?[0-9]+),(\-?[0-9]+),(\-?[0-9]+)>, v=<(\-?[0-9]+),(\-?[0-9]+),(\-?[0-9]+)>, a=<(\-?[0-9]+),(\-?[0-9]+),(\-?[0-9]+)>', particleConfig)

    if matchObj == None:
        print("Could not parse particle config:", particleConfig)
        continue

    velocity = Vector3(int(matchObj.group(4)), int(matchObj.group(5)), int(matchObj.group(6)))
    acceleration = Vector3(int(matchObj.group(7)), int(matchObj.group(8)), int(matchObj.group(9)))

    velocityML = velocity.get_manhattan_length()
    accelerationML = acceleration.get_manhattan_length()
    
    if smallestAccelerationML >= accelerationML:
        if smallestAccelerationML > accelerationML:
            smallestAccelerationML = accelerationML
            smallestAccelerationVelocityML = velocityML
            smallestAcceleratedParticle = currentParticle
        else: # ==
            if smallestAccelerationVelocityML >= velocityML:
                if smallestAccelerationVelocityML > velocityML:
                    smallestAccelerationManhattanLength = accelerationML
                    smallestAccelerationVelocityML = velocityML
                    smallestAcceleratedParticle = currentParticle
                else: # ==
                    print("Does this happen? If so consider position as well")

    currentParticle += 1

print("Answer: particle ", smallestAcceleratedParticle, "with acceleration", smallestAccelerationManhattanLength)
