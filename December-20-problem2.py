# Particle Swarm
import re
import math

filename = "December-20-input.txt"
file = open(filename, "r")
input = file.read()
file.close()

lines = input.split("\n")

class Vector3:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    # Used for debugging. This method is called when you print an instance  
    def __str__(self):
        return "(" + str(self.x) + ", " + str(self.y) + ", " + str(self.z) + ")"

    def get_length(self):
        return math.sqrt(self.x ** 2 + self.y ** 2 + self.z ** 2)

    def __add__(self, v):
        return Vector3(self.x + v.x, self.y + v.y, self.z + v.z)

    def __sub__(self, v):
        return Vector3(self.x - v.x, self.y - v.y, self.z - v.z)

    def __mul__(self, n):
        return Vector3(self.x * n, self.y * n, self.z * n)
    
    def get_manhattan_length(self):
        return abs(self.x) + abs(self.y) + abs(self.z)
    
    def __eq__(self, other): 
        return self.x == other.x and self.y == other.y and self.z == other.z
    
class Particle:
    def __init__(self, position, velocity, acceleration):
        self.position = position
        self.velocity = velocity
        self.acceleration = acceleration
        self.collided = False
    
    def step(self):
        self.velocity = self.velocity + self.acceleration
        self.position = self.position + self.velocity
        
particles = []

# parse particle config
for particleConfig in lines:
    matchObj = re.match( r'p=<(\-?[0-9]+),(\-?[0-9]+),(\-?[0-9]+)>, v=<(\-?[0-9]+),(\-?[0-9]+),(\-?[0-9]+)>, a=<(\-?[0-9]+),(\-?[0-9]+),(\-?[0-9]+)>', particleConfig)

    if matchObj == None:
        print("Could not parse particle config:", particleConfig)
        continue

    position = Vector3(int(matchObj.group(1)), int(matchObj.group(2)), int(matchObj.group(3)))
    velocity = Vector3(int(matchObj.group(4)), int(matchObj.group(5)), int(matchObj.group(6)))
    acceleration = Vector3(int(matchObj.group(7)), int(matchObj.group(8)), int(matchObj.group(9)))

    particle = Particle(position, velocity, acceleration)
    particles.append(particle)

# simulate
for steps in range(3000):
    # test for collisions
    hadCollisions = False
    for i in range(len(particles)):
        for j in range(i+1, len(particles)):
            if particles[i].position == particles[j].position:
                hadCollisions = True
                particles[i].collided = True
                particles[j].collided = True
                print("Collision between ", i, "and", j)
    
    if hadCollisions:
        i = 0
        while i < len(particles):
            if particles[i].collided:
                del particles[i]
            else:
                i += 1
        
    # step particles
    for particle in particles:
        particle.step()

print("Particles left:", len(particles))