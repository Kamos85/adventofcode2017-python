# Fractal Art
import math
import re

filename = "December-21-input.txt"
file = open(filename, "r")
input = file.read()
file.close()

rawRules = input.split("\n")

# test
# rawRules = ["../.# => ##./#../...", ".#./..#/### => #..#/..../..../#..#"]

startPattern = [".#.",
                "..#",
                "###"]
startPattern = "".join(startPattern)

class Rulebook:
    def __init__(self, rawRules):
        self.rules = self.createRulesFromInput(rawRules)
        print(len(rawRules), "->", len(self.rules))

    def createRulesFromInput(self, rawRules):
        rules = {}
        for rawRule in rawRules:
            rawRule = rawRule.replace("/", "")
            matchObj = re.match( r'(.*) => (.*)', rawRule)
            if matchObj == None:
                print("Could not parse rule:", rawRule)
                continue
            
            ruleInput = matchObj.group(1)
            ruleOutput = matchObj.group(2)
            
            rules[ruleInput] = ruleOutput
            ruleInput = self.rotateLeftRule(ruleInput)
            rules[ruleInput] = ruleOutput
            ruleInput = self.rotateLeftRule(ruleInput)
            rules[ruleInput] = ruleOutput
            ruleInput = self.rotateLeftRule(ruleInput)
            rules[ruleInput] = ruleOutput
            ruleInput = self.mirrorRule(ruleInput)
            rules[ruleInput] = ruleOutput
            ruleInput = self.rotateLeftRule(ruleInput)
            rules[ruleInput] = ruleOutput
            ruleInput = self.rotateLeftRule(ruleInput)
            rules[ruleInput] = ruleOutput
            ruleInput = self.rotateLeftRule(ruleInput)
            rules[ruleInput] = ruleOutput
        return rules
    
    def rotateLeftRule(self, ruleInput):
        newRuleInput = ["."] * len(ruleInput)
        if len(ruleInput) == 4:
            newRuleInput[0] = ruleInput[1]
            newRuleInput[1] = ruleInput[3]
            newRuleInput[2] = ruleInput[0]
            newRuleInput[3] = ruleInput[2]
        else: # len(ruleInput) == 9
            newRuleInput[0] = ruleInput[2]
            newRuleInput[1] = ruleInput[5]
            newRuleInput[2] = ruleInput[8]
            newRuleInput[3] = ruleInput[1]
            newRuleInput[4] = ruleInput[4]
            newRuleInput[5] = ruleInput[7]
            newRuleInput[6] = ruleInput[0]
            newRuleInput[7] = ruleInput[3]
            newRuleInput[8] = ruleInput[6]
        return "".join(newRuleInput)
        
    def mirrorRule(self, ruleInput):
        newRuleInput = ["."] * len(ruleInput)
        if len(ruleInput) == 4:
            newRuleInput[0] = ruleInput[1]
            newRuleInput[1] = ruleInput[0]
            newRuleInput[2] = ruleInput[3]
            newRuleInput[3] = ruleInput[2]
        else: # len(ruleInput) == 9
            newRuleInput[0] = ruleInput[2]
            newRuleInput[1] = ruleInput[1]
            newRuleInput[2] = ruleInput[0]
            newRuleInput[3] = ruleInput[5]
            newRuleInput[4] = ruleInput[4]
            newRuleInput[5] = ruleInput[3]
            newRuleInput[6] = ruleInput[8]
            newRuleInput[7] = ruleInput[7]
            newRuleInput[8] = ruleInput[6]
        return "".join(newRuleInput)
    
    def convertPattern(self, pattern):
        output = self.rules.get(pattern)
        if output == None:
            print("Pattern", pattern, "not found!")
            return ""

        return output
        
rulebook = Rulebook(rawRules)

pattern = startPattern

for steps in range(0,5):
    patternLength = len(pattern)
    patternSize = int(math.sqrt(patternLength))
    
    if (patternSize % 2) == 0:
        subPatternSize = 2
    else:
        if (patternSize % 3) != 0:
            print("patternSize:", patternSize, "not a multiple of 2 or 3!") #sanity check
        subPatternSize = 3

    newSubPatternSize = subPatternSize + 1
    patternsInSide = int(patternSize / subPatternSize)
    newPatternSize = patternSize + patternsInSide # each pattern is expanded by 1
    newPatternLength = newPatternSize * newPatternSize
    newPatternAsList = ["."] * newPatternLength # not as string because string can't be modified
    for y in range(patternsInSide):
        for x in range(patternsInSide):
            # construct subpattern
            oldSubPattern = ""
            for subY in range(0, subPatternSize):
                ySubPatternIndex = (y*subPatternSize + subY) * patternSize # map 2 dimension to 1
                yxSubPatternIndex = ySubPatternIndex + x*subPatternSize
                oldSubPattern += pattern[yxSubPatternIndex : yxSubPatternIndex + subPatternSize]
            newSubPattern = rulebook.convertPattern(oldSubPattern)
            
            for subY in range(0, newSubPatternSize):
                ySubPatternIndex = (y*newSubPatternSize + subY) * newPatternSize # map 2 dimension to 1
                yxSubPatternIndex = ySubPatternIndex + x*newSubPatternSize
                for subX in range(0,newSubPatternSize):
                    newPatternAsList[yxSubPatternIndex+subX] = newSubPattern[subY*newSubPatternSize + subX]
                
            #print("patr:", oldSubPattern, newSubPattern)
    
    pattern = "".join(newPatternAsList)

print("Answer:", pattern.count("#"))
