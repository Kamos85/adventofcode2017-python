# Sporifica Virus

class Direction:
    UP = 1
    RIGHT = 2
    DOWN = 3
    LEFT = 4
    
    def turnRight(direction):
        direction += 1
        if direction == 5:
            direction = 1
        return direction
    
    def turnLeft(direction):
        direction -= 1
        if direction == 0:
            direction = 4
        return direction

filename = "December-22-input.txt"
file = open(filename, "r")
input = file.read()
file.close()

stringBoard = input.split("\n")

# test
##stringBoard = ["..#",
##               "#..",
##               "..."]

padding = 150

paddingSide = "." * padding
paddingRowWithoutSide = "." * len(stringBoard[0])
# pad bottom and top
for i in range(padding):
    stringBoard.insert(0, paddingRowWithoutSide)
    stringBoard.insert(len(stringBoard), paddingRowWithoutSide)
# pad sides
for i in range(len(stringBoard)):
    stringBoard[i] = paddingSide + stringBoard[i] + paddingSide

board = list()
for stringRow in stringBoard:
    board.append(list(stringRow))

currentX = int(len(board[0])/2)
currentY = int(len(board)/2)
currentDirection = Direction.UP

totalInfections = 0
for burst in range(0,10000):
    infected = (board[currentY][currentX] == "#")
    if infected:
        currentDirection = Direction.turnRight(currentDirection)
        board[currentY][currentX] = "."
    else: # not infected
        currentDirection = Direction.turnLeft(currentDirection)
        board[currentY][currentX] = "#"
        totalInfections += 1
    # move forward
    if currentDirection == Direction.UP:
        currentY -= 1
    if currentDirection == Direction.RIGHT:
        currentX += 1
    if currentDirection == Direction.DOWN:
        currentY += 1
    if currentDirection == Direction.LEFT:
        currentX -= 1
        
#for row in board:
#    print("".join(row))

print(totalInfections)
