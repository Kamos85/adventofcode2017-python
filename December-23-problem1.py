# Coprocessor Conflagration
import re

filename = "December-23-input.txt"
file = open(filename, "r")
input = file.read()
file.close()

input = input.split("\n")

registers = {}

instructionCounter = 0
playedFrequency = 0

mulInvokeCount = 0

# function to convert a string like "123" to an int or if the
# operant is a register name returns the value of the register
def getOperantAsValue(stringOperant):
    global registers
    try:
        value = int(stringOperant)
    except:
        value = registers.get(stringOperant, 0)
    return value
    

while instructionCounter >= 0 and instructionCounter < len(input):
    matchObj = re.match( r'(set|sub|mul|jnz) (\-?[0-9a-z]+).?(\-?[0-9a-z]+)?', input[instructionCounter])
    
    if matchObj == None:
        print("Could not parse instruction:", instruction)
        continue

    opcode = matchObj.group(1)
    operant1 = matchObj.group(2)
    operant2 = matchObj.group(3)
    #print("executing:", opcode, operant1, operant2)

    if opcode == "set":
        registers[operant1] = getOperantAsValue(operant2)
        instructionCounter += 1
        continue

    if opcode == "sub":
        registerValue = registers.get(operant1, 0)
        registerValue -= getOperantAsValue(operant2)
        registers[operant1] = registerValue
        instructionCounter += 1
        continue

    if opcode == "mul":
        registerValue = registers.get(operant1, 0)
        registerValue *= getOperantAsValue(operant2)
        registers[operant1] = registerValue
        instructionCounter += 1
        mulInvokeCount += 1
        continue
    
    if opcode == "jnz":
        conditionValue = getOperantAsValue(operant1)
        if conditionValue != 0:
            instructionCounter += getOperantAsValue(operant2)
        else:
            instructionCounter += 1
        continue;
    
print("Answer:", mulInvokeCount)
