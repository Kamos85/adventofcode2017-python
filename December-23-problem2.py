# Coprocessor Conflagration
import math

a = 1
b = 67
c = 67
h = 0
if a != 0:
    b = 106700
    c = 123700

def IsPrime(number):
    sqrtNumber = math.ceil(math.sqrt(number)) + 1
    for i in range(2, sqrtNumber):
        if not number % i:
            return False
    return True

while True:
    if not IsPrime(b):
        h += 1
    if b == c:
        break
    b += 17

print("Answer", h)