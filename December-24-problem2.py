# Electromagnetic Moat
import re

PORT_USED = 0
PORT_1 = 1
PORT_2 = 2

def readAndProcessInput():
    global components
    
    filename = "December-24-input.txt"
    file = open(filename, "r")
    input = file.read()
    file.close()

    input = input.split("\n")

    for entry in input:
        matchObj = re.match( r'([0-9]+)/([0-9]+)', entry)

        if matchObj == None:
            print("Could not parse component:", entry)
            continue

        port1 = int(matchObj.group(1))
        port2 = int(matchObj.group(2))

        components.append([False, port1, port2])

def getFreeComponentWithPort(port):
    global components
    for component in components:
        if not component[PORT_USED]:
            if component[PORT_1] == port or component[PORT_2] == port:
                return component
    return None

def getFreeComponentsWithPort(port):
    global components
    result = []
    for component in components:
        if not component[PORT_USED]:
            if component[PORT_1] == port or component[PORT_2] == port:
                result.append(component)
    return result

def computeCurrentBridgeStrength():
    global currentBridge
    strength = 0
    for component in currentBridge:
        strength += component[PORT_1]
        strength += component[PORT_2]
    return strength

def buildBridge(lastEndPort):
    global currentBridge
    global longestBridgePoints
    global longestBridgeStrengthPoints
    nextValidComponents = getFreeComponentsWithPort(lastEndPort)
        
    if len(nextValidComponents) == 0: # cannot build further
        if longestBridgePoints == len(currentBridge):
            # pick the strongest
            bridgeStrength = computeCurrentBridgeStrength()
            if longestBridgeStrengthPoints < bridgeStrength:
                longestBridgeStrengthPoints = bridgeStrength
        
        if longestBridgePoints < len(currentBridge):
            longestBridgePoints = len(currentBridge)
            longestBridgeStrengthPoints = computeCurrentBridgeStrength()
        return
    
    for nextComponent in nextValidComponents:
        currentBridge.append(nextComponent)
        nextComponent[PORT_USED] = True
        nextComponentEndPort = nextComponent[PORT_2] if nextComponent[PORT_1] == lastEndPort else nextComponent[PORT_1]
        buildBridge(nextComponentEndPort)
        currentBridge.pop()
        nextComponent[PORT_USED] = False

components = []
readAndProcessInput()

longestBridgePoints = 0
longestBridgeStrengthPoints = 0
currentBridge = []

startComponents = getFreeComponentsWithPort(0)

for startComponent in startComponents:
    currentBridge.append(startComponent)
    startComponent[PORT_USED] = True
    endPort = startComponent[PORT_2] if startComponent[PORT_1] == 0 else startComponent[PORT_1]
    buildBridge(endPort)
    currentBridge.pop()
    startComponent[PORT_USED] = False

print("Answer", longestBridgeStrengthPoints)
