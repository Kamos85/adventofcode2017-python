# The Halting Problem

# rewrote the given rules in a data structure (State) of my own
# seeing as in all problems the input doesn't change it seems a bit wasteful to write a parser for the input

class State:
    def __init__(self, writeIf0, moveIf0, nextStateIf0, writeIf1, moveIf1, nextStateIf1):
        self.writeIf0 = writeIf0
        self.moveIf0 = moveIf0
        self.nextStateIf0 = nextStateIf0
        self.writeIf1 = writeIf1
        self.moveIf1 = moveIf1
        self.nextStateIf1 = nextStateIf1

A = 0
B = 1
C = 2
D = 3
E = 4
F = 5

LEFT = -1
RIGHT = 1

states = [
    #A
    State(1, RIGHT, B, 0,  LEFT, D),
    #B
    State(1, RIGHT, C, 0, RIGHT, F),
    #C
    State(1,  LEFT, C, 1,  LEFT, A),
    #D
    State(0,  LEFT, E, 1, RIGHT, A),
    #E
    State(1,  LEFT, A, 0, RIGHT, B),
    #F
    State(0, RIGHT, C, 0, RIGHT, E)
]

tape = [0]
cursor = 0

checkSumAfterSteps = 12302209

def moveCursor(direction):
    global tape
    global cursor
    cursor += direction
    if cursor == -1: #expand tape at start
        tape.insert(0, 0)
        cursor = 0
    if cursor == len(tape): #expand tape at end
        tape.append(0)

# Simulate Turing machine
currentState = states[A]

while checkSumAfterSteps != 0:
    currentValue = tape[cursor]
    if currentValue == 0:
        tape[cursor] = currentState.writeIf0
        moveCursor(currentState.moveIf0)
        currentState = states[currentState.nextStateIf0]
    else: #1
        tape[cursor] = currentState.writeIf1
        moveCursor(currentState.moveIf1)
        currentState = states[currentState.nextStateIf1]
    checkSumAfterSteps -= 1

checksum = tape.count(1)
print("Answer", checksum)
